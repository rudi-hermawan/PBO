
public class LatihanConstructor {
    String npm,nama;
    
    void setNPM(String npm){
        this.npm = npm;
    }
    
    String getNPM(){
        return this.npm;
    }
    void setNama(String nama){
        this.nama = nama;
    }
    
    String getNama(){
        return this.nama;
    }
    public static void main(String[] args) {
        LatihanConstructor lc = new LatihanConstructor();
        lc.setNPM("021200089");
        System.out.println(lc.getNPM());
        lc.setNama("Rudi Hermawan");
        System.out.println(lc.getNama());
    }
}
